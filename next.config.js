/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental: {
    outputStandalone: true,
  },
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  excludeFile: (str) => /\*.{spec,test}.js/.test(str),
};

module.exports = nextConfig;
