# 🤝 Contribution Guide

## 🏗️ Development environment

In addition to the previous [requirements](#🛠️-requirements) you will also need [NodeJS](https://nodejs.org/en/) `16.*` to start developing locally (using docker only for the database).

### ▶️ Start the development environment

```bash
# Install development dependencies
npm ci
# Start the database and run the migrations
# NOTE: use "docker-compose" when using compose 1.x
docker compose run --rm migrations
# Run the development server
npm run dev
```

After the commands exit successfully, the application should be available at http://localhost:3000

### ✨ Lint and format

Linting and formatting is done with a combination of [eslint](https://eslint.org/), [stylelint](https://stylelint.io/) and [prettier](https://prettier.io/)

```bash
# Check files are linted and formatted correctly
npm run lint
# Lint and format files
npm run lint:fix
```

(See [`package.json`](package.json) for more specific lint commands)

### 🎭 Test

Currently only end to end tests are implemented with cypress (see [Roadmap](#🗺️-roadmap)):

```bash
# Run Cypress test suite in a non-headless environment
npm run cypress
```

### 🐋 Build

The recommended method to build this application is to build the docker image, but you can also transpile it into "shippable" javascript:

```bash
# Build production docker image
docker build . --target prod
# Transpile into javascript
npm run build
```

(See [`package.json`](package.json) for more specific lint commands)
