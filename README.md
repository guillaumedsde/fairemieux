<div align="center">
  <img width="250px" src="https://gitlab.com/guillaumedsde/fairemieux/-/wikis/uploads/bbfd087b1132b6ad01b355bb064aeb15/demo.gif" alt="FaireMieux demo">
  <p>
  FaireMieux is a simple TODO list application written with <a href="https://nextjs.org/">NextJS</a> using the <a href ="https://www.apollographql.com/">Apollo</a> GraphQL server and client.
  </p>
</div>

## 🛠️ Requirements

- [Docker](https://docs.docker.com/get-docker/) `19.03.0+`
  - (required for [compose specification support](https://docs.docker.com/compose/compose-file/compose-file-v3/#compose-and-docker-compatibility-matrix))
- [docker-compose](https://docs.docker.com/compose/install/) `1.27.0+` (`2.*` is supported)
  - (required for [compose specification support](https://docs.docker.com/compose/compose-file/compose-file-v3/#compose-and-docker-compatibility-matrix))

## 🚀 Getting Started

To start the application please clone the repository and run the application with docker compose:

```bash
docker compose up -d
```

After the command exits successfully, the application should be available at http://localhost:3000

_note: use `docker-compose` with docker-compose `1.*`_

## 🏗️ Development environment

See [`CONTRIBUTING.md`](CONTRIBUTING.md).

## 🗺️ Roadmap

- [x] Create TODO element
- [x] Mark TODO element as done
- [x] Delete TODO element
- [x] Configure formatters and linters
- [x] CI pipeline
- [ ] GraphQL endpoint integration tests
- [x] End to End (e2e) Tests
  - [ ] Improve e2e tests maintainability
- [ ] Backend queries error handling
  - [x] when fetching list of all todos
  - [ ] when creating todo
  - [ ] when marking todo as done
  - [ ] when deleting todo
- [ ] Pagination of TODO list (with infinite scrolling on the frontend?)
- [ ] Better TODO list refresh strategy (using GraphQL subscriptions?)
- [ ] Editable TODO element title
- [ ] Parse GraphQL query to only retrieve requested fields in SQL query
- [ ] Tweak Webpack configuration to improve bundle size?

## 📚 Resources

- https://vercel.com/guides/nextjs-prisma-postgres
- https://www.prisma.io/blog/fullstack-nextjs-graphql-prisma-oklidw1rhw
