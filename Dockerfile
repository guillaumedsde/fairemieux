ARG NODE_VERSION=16.14
ARG DEBIAN_VERSION=bullseye

FROM node:$NODE_VERSION-$DEBIAN_VERSION-slim AS deps

WORKDIR /app
COPY package*.json ./
RUN npm ci

COPY prisma/ ./
RUN npx prisma generate

FROM node:$NODE_VERSION-$DEBIAN_VERSION-slim AS builder

WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

ENV NEXT_TELEMETRY_DISABLED=1
ENV CHECKPOINT_DISABLE=1
ENV NODE_ENV=production
    
RUN npm run build

RUN ls .next

FROM node:$NODE_VERSION-$DEBIAN_VERSION-slim AS prod
WORKDIR /app

ENV NEXT_TELEMETRY_DISABLED=1
ENV CHECKPOINT_DISABLE=1
ENV NODE_ENV=production

RUN addgroup --system --gid 1001 nodejs && \ 
    adduser --system --uid 1001 nextjs

COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/package.json ./package.json

# Automatically leverage output traces to reduce image size 
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

EXPOSE 3000

ENV PORT 3000

CMD ["node", "server.js"]
