// gotten from https://stackoverflow.com/a/69211058
// to improve on https://www.prisma.io/docs/support/help-articles/nextjs-prisma-client-dev-practices#solution
import { PrismaClient } from "@prisma/client";

interface CustomNodeJsGlobal {
  prisma: PrismaClient;
}

declare const global: CustomNodeJsGlobal;

const prisma = global.prisma || new PrismaClient();

if (process.env.NODE_ENV === "development") global.prisma = prisma;

export default prisma;
