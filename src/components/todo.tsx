import Checkbox from "@mui/material/Checkbox";
import { gql, useMutation } from "@apollo/client";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DeleteTodo from "./deleteTodo";
import { completeTodoQuery } from "../graphql/queries";

export default function Todo({ todo }) {
  const [completeTodo, { loading }] = useMutation(completeTodoQuery, {
    update(cache, { data: { updateTodo } }) {
      cache.writeFragment({
        // NOTE: apollo graphql type name has two underscores
        // eslint-disable-next-line no-underscore-dangle
        id: `${updateTodo.__typename}:${updateTodo.id}`,
        fragment: gql`
          fragment MyTodo on Todo {
            done
          }
        `,
        data: {
          done: updateTodo.done,
        },
      });
    },
  });

  return (
    <ListItem secondaryAction={<DeleteTodo todo={todo} disabled={loading} />}>
      <ListItemIcon>
        <Checkbox
          id={`${todo.id}-done-checkbox`}
          checked={todo.done}
          disabled={loading}
          onChange={(event) => {
            completeTodo({
              variables: { id: todo.id, done: event.target.checked },
            });
          }}
        />
      </ListItemIcon>
      <ListItemText
        primaryTypographyProps={{
          style: {
            textDecoration: todo.done ? "line-through" : null,
          },
        }}
      >
        <label htmlFor={`${todo.id}-done-checkbox`}>{todo.title}</label>
      </ListItemText>
    </ListItem>
  );
}
