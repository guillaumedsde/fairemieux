import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import { useMutation } from "@apollo/client";
import { deleteTodoQuery } from "../graphql/queries";

export default function DeleteTodo({ disabled, todo }) {
  const [deleteTodo, { loading }] = useMutation(deleteTodoQuery, {
    update(cache, { data }) {
      cache.evict({ id: cache.identify(data.deleteTodo) });
      cache.gc();
    },
  });

  const handleOnClick = async () => {
    deleteTodo({
      variables: { id: todo.id },
    });
  };

  return (
    <IconButton
      aria-label="delete"
      disabled={disabled || loading}
      onClick={handleOnClick}
    >
      <DeleteIcon />
    </IconButton>
  );
}
