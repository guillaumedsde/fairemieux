import { Grid } from "@mui/material";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";

export default function Layout({ children }) {
  return (
    <Container maxWidth="sm" style={{ marginTop: "50px" }}>
      <Grid container direction="column" spacing={2}>
        <Grid
          container
          item
          justifyContent="center"
          alignItems="center"
          direction="column"
        >
          <Typography variant="h3" component="div">
            ✅ FaireMieux
          </Typography>
          <Typography variant="subtitle1" component="div" gutterBottom>
            A simple TODO application
          </Typography>
        </Grid>
        <Grid item>{children}</Grid>
      </Grid>
    </Container>
  );
}
