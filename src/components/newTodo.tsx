import TextField from "@mui/material/TextField";
import { useMutation } from "@apollo/client";
import { useState } from "react";

import { allTodosQuery, createTodoQuery } from "../graphql/queries";

export default function NewTodo() {
  const [createTodo, { loading }] = useMutation(createTodoQuery, {
    update(cache, { data }) {
      const { todos } = cache.readQuery({ query: allTodosQuery });
      cache.writeQuery({
        query: allTodosQuery,
        data: {
          todos: [...todos, data.createTodo],
        },
      });
    },
  });
  const [value, setValue] = useState("");

  return (
    <TextField
      value={value}
      onChange={(e) => setValue(e.target.value)}
      fullWidth
      autoFocus
      InputProps={{ disableUnderline: true }}
      variant="standard"
      placeholder="Enter a new thing to do"
      onKeyPress={async (event) => {
        if (event.key === "Enter" && !loading) {
          await createTodo({
            variables: { title: value },
          });
          setValue("");
        }
      }}
    />
  );
}
