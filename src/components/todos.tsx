import { useQuery } from "@apollo/client";
import { useEffect } from "react";
import LinearProgress from "@mui/material/LinearProgress";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import List from "@mui/material/List";
import NewTodo from "./newTodo";
import Todo from "./todo";
import { allTodosQuery } from "../graphql/queries";

const TODOS_UPDATE_POLLING_RATE = 3_000;

export default function Todos() {
  const { data, loading, error, startPolling, stopPolling } =
    useQuery(allTodosQuery);

  useEffect(() => {
    startPolling(TODOS_UPDATE_POLLING_RATE);
    return () => {
      stopPolling();
    };
  }, [startPolling, stopPolling]);

  if (loading) return <LinearProgress />;
  if (error)
    return (
      <Alert severity="error">
        <AlertTitle>Could not load current TODOs</AlertTitle>
        {error.message}
      </Alert>
    );

  return (
    <>
      <NewTodo />
      <List>
        {data.todos.map((todo) => (
          <Todo key={todo.id} todo={todo} />
        ))}
      </List>
    </>
  );
}
