import "../styles/globals.css";
import { ApolloProvider } from "@apollo/client";
import Head from "next/head";
import apolloClient from "../lib/apollo";
import Layout from "../components/layout";

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={apolloClient}>
      <Head>
        <title>FaireMieux</title>
        <meta
          name="description"
          content="FaireMieux - A simple TODO application"
        />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <Layout>
        <Component
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...pageProps}
        />
      </Layout>
    </ApolloProvider>
  );
}

export default MyApp;
