import { gql } from "apollo-server-micro";

export const typeDefs = gql`
  type Todo {
    id: Int
    title: String
    done: Boolean
  }

  type Query {
    todos: [Todo]!
    todo(id: Int!): Todo!
  }

  type Mutation {
    createTodo(title: String!, done: Boolean): Todo!
    updateTodo(id: Int!, done: Boolean, title: String): Todo!
    deleteTodo(id: Int!): Todo!
  }
`;
