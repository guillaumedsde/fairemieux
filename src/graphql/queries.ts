import { gql } from "@apollo/client";

export const allTodosQuery = gql`
  query {
    todos {
      id
      title
      done
    }
  }
`;

export const createTodoQuery = gql`
  mutation CreateTodo($title: String!) {
    createTodo(title: $title) {
      id
      title
      done
    }
  }
`;

export const completeTodoQuery = gql`
  mutation UpdateTodo($id: Int!, $done: Boolean!) {
    updateTodo(id: $id, done: $done) {
      id
      done
    }
  }
`;

export const deleteTodoQuery = gql`
  mutation DeleteTodo($id: Int!) {
    deleteTodo(id: $id) {
      id
    }
  }
`;
