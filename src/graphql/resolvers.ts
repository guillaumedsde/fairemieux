export const resolvers = {
  Query: {
    todos: (_parent, _args, ctx) => {
      return ctx.prisma.todo.findMany({
        orderBy: [
          {
            createdAt: "asc",
          },
        ],
      });
    },
  },
  Mutation: {
    createTodo: (_, data, ctx) => {
      return ctx.prisma.todo.create({
        data,
      });
    },
    updateTodo: (_, { id, ...data }, ctx) => {
      return ctx.prisma.todo.update({
        where: { id },
        data,
      });
    },
    deleteTodo: (_, { id }, ctx) => {
      return ctx.prisma.todo.delete({
        where: { id },
      });
    },
  },
};
