/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe("TODO app", () => {
  before(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("http://localhost:3000");
  });

  it("can add new todo items", () => {
    const newItem = "Feed the cat";

    cy.get(".MuiInput-input").type(`${newItem}{enter}`);

    cy.get(".MuiListItem-root").last().find("label").should("contain", newItem);
  });

  it("can mark todo items as done", () => {
    cy.get(".MuiListItem-root")
      .last()
      .find('[type="checkbox"]')
      .check()
      .should("be.checked");
  });
  it("todo items marked as done are crossed", () => {
    cy.get(".MuiListItemText-root > .MuiTypography-root")
      .last()
      .should("have.css", "text-decoration-line", "line-through");
  });
  it("can delete todo items", () => {
    cy.get(".MuiListItem-root")
      .last()
      .find("button")
      .click()
      .should("not.exist");
  });
});
